<?php

 require 'vendor/autoload.php';

// Implement the Container interface (out of scope for example)
require 'container.php';
SAML2\Compat\ContainerSingleton::setContainer($container);

// Create Issuer
$issuer = new SAML2\XML\saml\Issuer('https://sp.example.edu');

// Set up an AuthnRequest
$request = new SAML2\XML\samlp\AuthnRequest(
    $issuer,
    $container->generateId(),
    null,
    'https://idp.example.edu'
);

// Send it off using the HTTP-Redirect binding
$binding = new SAML2\HTTPRedirect();
$binding->send($request);