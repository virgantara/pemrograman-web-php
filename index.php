<?php 
session_start();

require_once "db.php";

$sql = "SELECT * FROM mahasiswa";

$hasil = $conn->query($sql);

// Fungsinya hanya untuk mengecek session-nya ada atau tidak
if(!empty($_SESSION['username'])) 
{
	$nama = $_SESSION['username'];
	echo 'Selamat datang, wahai '.$nama;	
}


?><br>
1. <a href="login.php">Login</a><br>
2. <a href="logout.php">Logout</a><br>
<br><br>
<table border="1" cellpadding="3">
	<tr>
		<th>Nomor</th>
		<th>NIM</th>
		<th>Nama</th>
		<th>Alamat</th>
		<th>Aksi</th>
	</tr>

	<?php 

	$nomor = 0;
	while($row = $hasil->fetch_assoc())
	{
		$nomor++;

	?>
	<tr>
		<td><?=$nomor;?></td>
		<td><?=$row['nim'];?></td>
		<td><?=$row['nama'];?></td>
		<td><?=$row['alamat'];?></td>
		<td>
			<a href="update.php?nim=<?=$row['nim'];?>">Update</a>
			<a href="process_delete.php">Hapus</a>
		</td>
	</tr>
	<?php 
	// ini komen php
	}
	?>
</table>
<!-- ini komen html-->
<a href="create.php">Tambah Data</a>